job "asset-management" {
  name = "Asset Management (Snipe-IT)"
  type = "service"
  region = "[[ .defaultRegion ]]"
  datacenters = ["[[ .defaultDatacenter ]]"]
  priority = "[[ .defaultPriority ]]"
  namespace = "assets"

  vault {
    policies = ["job-asset-management"]
  }

  group "snipe-it" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "snipe-it"
      port = "80"
      task = "snipe-it"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "mysql"
              local_bind_port  = 3306
            }

            upstreams {
              destination_name = "loki"
              local_bind_port  = 3100
            }
          }
        }
      }

      check {
        expose = true
        name = "Application Health Status"
        type = "http"
        path = "/health"
        interval = "10s"
        timeout = "3s"

        check_restart {
          limit = 3
          grace = "90s"
          ignore_warnings = false
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.snipe.entrypoints=https",
        "traefik.http.routers.snipe.rule=Host(`snipe.hq.carboncollins.se`)",
        "traefik.http.routers.snipe.tls=true",
        "traefik.http.routers.snipe.tls.certresolver=lets-encrypt",
        "traefik.http.routers.snipe.tls.domains[0].main=*.hq.carboncollins.se"
      ]
    }

    volume "snipe-data" {
      type = "csi"
      source = "snipe-data"
      read_only = false
      attachment_mode = "file-system"
      access_mode = "single-node-writer"
    }

    task "snipe-it" {
      driver = "docker"
      leader = true

      config {
        image = "[[ .snipeitImage ]]"
      }

      resources {
        cpu = 250
        memory = 300
      }

      volume_mount {
        volume = "snipe-data"
        destination = "/config"
        read_only = false
      }

      env {
        TZ = "[[ .defaultTimezone ]]"
        PUID = "[[ .defaultUserId ]]"
        PGID = "[[ .defaultGroupId ]]"

        APP_ENV = "production"
        APP_TIMEZONE = "[[ .defaultTimezone ]]"
        APP_URL = "https://snipe.hq.carboncollins.se"
        APP_LOCALE = "en"
      }

      template {
        data = <<EOH
          {{ with secret "jobs/asset-management/tasks/snipe-it/mysql" }}
          MYSQL_DATABASE={{ index .Data.data "database" }}
          MYSQL_USER={{ index .Data.data "user" }}
          MYSQL_PASSWORD={{ index .Data.data "password" }}
          MYSQL_PORT_3306_TCP_ADDR={{ env "NOMAD_UPSTREAM_IP_mysql" }}
          MYSQL_PORT_3306_TCP_PORT={{ env "NOMAD_UPSTREAM_PORT_mysql" }}
          {{ end }}
        EOH

        destination = "secrets/mysql.env"
        env = true
      }
    }

    task "log-shipper-snipe-it" {
      driver = "docker"

      lifecycle {
        hook = "prestart"
        sidecar = true
      }

      config {
        image = "grafana/promtail:2.4.1"

        args = [
          "-config.file=${NOMAD_TASK_DIR}/promtail.yaml"
        ]
      }

      resources {
        cpu = 50
        memory = 75
      }

      template {
        data =<<EOH
[[ fileContents "./config/promtail.template.yaml" ]]
        EOH

        destination = "local/promtail.yaml"
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
