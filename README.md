# Asset Management

[Snipe-IT](https://snipeitapp.com/) asset management service

[[_TOC_]]

## Description

This repository contains all of the required configuration files to be able to run a Snipe-IT server
which is used to track assets within the hQ home. 

## Asset Tags

Each item within hQ is individually tagged with an ID number and a label which is physically attached.
This label has a QR code and the ID number which both link the item back to the snipe ui.
