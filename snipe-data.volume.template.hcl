id = "snipe-data"
name = "snipe-data"
type = "csi"
external_id = "snipe-data"
plugin_id = "axion-proxima-smb"
namespace = "assets"
capacity_max = "4G"
capacity_min = "1G"

capability {
  access_mode     = "single-node-reader-only"
  attachment_mode = "file-system"
}

capability {
  access_mode     = "single-node-writer"
  attachment_mode = "file-system"
}

mount_options {
  fs_type = "cifs"
  mount_flags = [
    "username=[[ .volumeUser ]]",
    "password=[[ .volumePass ]]",
    "vers=3",
    "uid=[[ .defaultUserId ]]",
    "gid=[[ .defaultGroupId ]]",
    "nobrl"
  ]
}

secrets {
  username = "[[ .volumeUser ]]"
  password = "[[ .volumePass ]]"
}

context {
  node_attach_driver = "smb"
  provisioner_driver = "smb-driver"
  server = "[[ .axionServer ]]"
  share = "[[ .axionShare ]]"
}
